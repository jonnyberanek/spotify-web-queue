const express = require('express');
const app = express();

const request = require('request');
const cors = require('cors');
const querystring = require('querystring');
const cookieparser = require('cookie-parser');

const client_id = "33534b761567483cb214a2df51e34373";
const client_secret = "fafe4248224843bdaa968973e4c17cb2";
const redirect_uri = "http://localhost:8888/callback";

var scopes = 'user-read-private user-read-email';

const stateKey = 'spotify_auth_state';

var generateRandomString = function(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

app.use(express.static(__dirname + '/public'))
   .use(cors())
   .use(cookieparser());

app.get('/login', (req, res) => {
	
	var state = generateRandomString(16);
	res.cookie(stateKey, state);
	
	res.redirect('https://accounts.spotify.com/authorize?'+
				querystring.stringify({
		response_type: 'code',
		client_id: client_id,
		scope: scopes,
		redirect_uri: redirect_uri,
		state: state
	}));
});



app.get('/callback', (req, res) => {
	
	var code = req.query.code || null;
	var state = req.query.state || null;
	var storedState = req.cookies ? req.cookies[stateKey] : null;
	
	if(state === null || state !== storedState){
		res.redirect('/#'+querystring.stringify({
			error: 'state_mismatch'
		}));
	}
	else{
		res.clearCookie(stateKey);
		var authOptions = {
			url: 'https://accounts.spotify.com/api/token',
			form: {
				code: code,
				redirect_uri: redirect_uri,
				grant_type: 'authorization_code'
			},
			headers: {
				'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
			},
			json: true
		};

		request.post(authOptions, function(error, response, body) {
			if(!error && response.statusCode == 200){
				var access_token = body.access_token;
				var refresh_token = body.refresh_token;

				var options = {
					url: 'https://api.spotify.com/v1/me',
					headers: {'Authorization': 'Bearer' + access_token},
					json: true
				};

				request.get(options, function(error, response, body){
					console.log(body);
				});

				res.redirect('/#' + querystring.stringify({
					access_token: access_token,
					refresh_token: refresh_token
				}));

			}
			else{
				res.redirect('/#' + querystring.stringify({
					error: 'invalid_token'
				}));
			}
		});
	}
});

app.get('/refresh_token', function(req, res) {

  // requesting access token from refresh token
  var refresh_token = req.query.refresh_token;
  var authOptions = {
    url: 'https://accounts.spotify.com/api/token',
    headers: { 'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64')) },
    form: {
      grant_type: 'refresh_token',
      refresh_token: refresh_token
    },
    json: true
  };

  request.post(authOptions, function(error, response, body) {
    if (!error && response.statusCode === 200) {
      var access_token = body.access_token;
      res.send({
        'access_token': access_token
      });
    }
  });
});

app.listen(8888, () => {
  console.log('Example app listening on port 8888!')
});