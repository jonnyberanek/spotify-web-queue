var access_token;
var selected_playlist = null;
var current_songs = [];
var selectedPlaylistElement = null;

function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		vars[key] = value;
	});
	return vars;
}

function makePlaylist(arr, name){
    var playlistId = "";
    $.ajax({
        url:"https://api.spotify.com/v1/me/playlists",         
        method: "POST",
        headers: {
				'Authorization': 'Bearer ' + access_token
        },
        data: JSON.stringify({
            "name": name
        }),
        contentType: 'application/json',
        success: function(response){
            var parsed = JSON.parse(JSON.stringify(response));
            playlistId = parsed.id;
            var idConcat = "";
            arr.forEach(function(track){
                idConcat += "spotify%3Atrack%3A"+track.id+"%2C";
            });
            idConcat = idConcat.slice(0,idConcat.length-3);
            $.ajax({
                url: "https://api.spotify.com/v1/playlists/" + playlistId + "/tracks?uris=" + idConcat,
                method: "POST",
                contentType: 'application/json',
                headers: {
                        'Authorization': 'Bearer ' + access_token
                },
                success: function(response){
                    alert("Big succ! Check Spotify");
                }
            });

        },
        error: function(error){
            alert("fuckin error");
        }
    });

}

//returns a sine wave fitted array assuming the input is sorted
function cosFit(arr, feature, numCurves){
    // defaulting to a 4-way split, may variablize later
    
    //'MEDIAN'-BASED SPLIT (equal sections split)
    
    //assuming 4 for now
    var numBuckets = 4;
    var subLength = Math.floor(arr.length/numBuckets);
    var overflow = arr.length%numBuckets;
    var buckets = [];
    var count = 0;
    for(var i = 0; i < numBuckets; i++){
        buckets.push([]);
        for(var j = 0; j < subLength; j++){
            buckets[i].push(count);
            count++;
        }
        if(overflow > 0){ 
            buckets[i].push(count);
            count++;
            overflow--;
        }
    }
    console.log(buckets);
    
    var waveArray = [];
    
    for(var i = 0; i < numCurves; i++){  
        var kMax = subLength/numCurves/2;
        //populate downward, 
        for(var j = numBuckets-1; j >= 0; j--){
            for(var k = 0; k < kMax; k++){
                if(buckets[j].length){ //stops from adding nothing/crashing
                    waveArray.push(arr[buckets[j].pop()]);
                }
            }
        }
        //populate upward
        for(var j = 0; j < numBuckets; j++){
            for(var k = 0; k < kMax; k++){
                if(buckets[j].length){ //stops from adding nothing/crashing
                    waveArray.push(arr[buckets[j].pop()]);
                }
            }
        }
    }
    return waveArray;
}

function buildGraph(arr, feature){
    var sum = 0;
    var coordinates = [{ x: [], y: []}]
    for(var i = 0; i < arr.length; i++){
        sum += arr[i].features[feature];
        coordinates[0].x.push(i);
        coordinates[0].y.push(arr[i].features[feature]);
    }
    var avg = sum/arr.length;
    var popVar = 0; 
    for(var i = 0; i < arr.length; i++){
        popVar += Math.pow(avg - arr[i].features[feature], 2);
    }
    popVar /= arr.length;
    Plotly.newPlot( $("#tester")[0], coordinates, {
	margin: { t: 0 } } );
    $("#playlist_stats").empty();
    $("#playlist_stats").append("<li> Average: "+ avg +"</li>");
    $("#playlist_stats").append("<li> Min: "+ arr[0].features[feature] +"</li>");
    $("#playlist_stats").append("<li> Max: "+ arr[arr.length-1].features[feature] +"</li>");
    var range = arr[arr.length-1].features[feature]-arr[0].features[feature];
    $("#playlist_stats").append("<li> Range: "+ range +"</li>");
    $("#playlist_stats").append("<li> Standard Deviation: "+ Math.sqrt(popVar) +"</li>");
    
}

/*
//function quickSortByFeature(arr, low, high, targetFeature){
//	if(low < high){
//		var p = partitionByFeature(arr, low, high, targetFeature);
//		quickSortByFeature(arr, low, p-1, targetFeature);
//		quickSortByFeature(arr, p, high, targetFeature);
//	}
//}
//function partitionByFeature(arr, low, high, targetFeature){
//		console.log(current_songs);
//	var pivot = arr[high].features[targetFeature];
//	var i = low-1;
//
//	for(var j = low; j <= high; j++){
//		if(arr[j].features[targetFeature]<=pivot){
//			i++;
//			var temp = arr[i];
//			arr[i] = arr[j];
//			arr[j] = temp;
//		}
//	}
//	var temp = arr[i+1];
//	arr[i+1] = arr[high];
//	arr[high] = temp;
//	return(i+1);
//}
*/

function flowify(){
	if(selected_playlist == null){
		alert("Please select a playlist");
	}
	else{
		// query tracks in playlist make array of songs in playlist
		// concat all songs and query

		current_songs = []; //todo : make this not like this, have all features store

		//outer ajax loads tracks in selected playlist, inner loads tracks' audio features
		$.ajax({
			url: "https://api.spotify.com/v1/playlists/" + selected_playlist + "/tracks?fields=items(track(name%2Cid))",
			headers: {
				'Authorization': 'Bearer ' + access_token
			},
			success: function(response){

				var id_concat = "";
				var parsed = JSON.parse(JSON.stringify(response));
				parsed.items.forEach(function(track){
					console.log(current_songs);
					current_songs.push({id: track.track.id, name: track.track.name, features:{}});
					id_concat += track.track.id+"%2C";
				});
				id_concat = id_concat.slice(0,id_concat.length-3); //corrects last %2C
				console.log(current_songs);
				//ajax to get audio features of tracks from selected playlist
				$.ajax({
					url: "https://api.spotify.com/v1/audio-features?ids=" + id_concat,
					headers: {
						'Authorization': 'Bearer ' + access_token
					},
					success: function(response){
                        $("#flow_results").empty();
						var selectedFeature = $("#features option:selected").val().toLowerCase();
						//$("#playlists").remove("li");

						var parsed = JSON.parse(JSON.stringify(response));
						console.log(current_songs);
						parsed.audio_features.forEach(function(track){

							current_songs.find(t => t.id === track.id).features[selectedFeature] = track[selectedFeature];
						});

						console.log(current_songs);
						//quickSortByFeature(current_songs, 0, current_songs.length-1, selectedFeature);
						current_songs.sort(function(a,b){
							return a.features[selectedFeature] - b.features[selectedFeature];
						});
						console.log(current_songs);
						console.log(current_songs[0].features[selectedFeature]);
						current_songs.forEach(function(track){
							var statted = $("<li><b>" + track.name + "</b> - "+ selectedFeature +": " + track.features[selectedFeature] + "</li>");

							$('#flow_results').append(statted);
						});

                        buildGraph(current_songs, selectedFeature);
                        
					}
				});
			}
		});

	}
}

$(document).ready(function(){
	var params = getUrlVars();
	access_token = params["access_token"];
	//ajax statement to grab playlists of current user
	$.ajax({
		url: 'https://api.spotify.com/v1/me/playlists?fields=items(name%2Cid)&limit=50',
		headers: {
			'Authorization': 'Bearer ' + access_token
		},
		success: function(response){
			var parsed = JSON.parse(JSON.stringify(response));
			parsed.items.forEach(function(element){
				var playlist = $("<li>" + element.name + "</li>");
				playlist.click(function(){
                    if(selectedPlaylistElement !== null){
                        selectedPlaylistElement.css("font-style","");
                        selectedPlaylistElement.css("font-weight","");
                    }
					selected_playlist = element.id;
                    selectedPlaylistElement = playlist;
                    selectedPlaylistElement.css("font-style","oblique")
                    selectedPlaylistElement.css("font-weight","bold");
				});

				$('#playlists').append(playlist);


			});
		},
		dataType: "json"

	});

});